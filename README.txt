Readme
------

The voting module is a simple five star voting/rating 
system that depends on the votingapi module.  Five star 
voting systems like this are often used for photo 
galleries, news articles, etc.  It uses an embedded 
Flash movie so that the page does not need to reload when 
a user votes.  The source .FLA file is included. 

The voting module can be used in a couple different ways.
* You can enable/disable it for different types of nodes.
* You can add a voting control anywhere by editing your
  theme file (or hacking a module).

This system works with nodes, comments, or any other type of 
content.  Since it uses the votingapi module it could also be
used with other votingapi based voting systems.  


Author
------
Benjamin Shell <drupal@benjaminshell.com>


Demo Sites
----------
http://www.benjaminshell.com/jokes
http://www.saccf.com/photos


TODO
----
- Add "un-vote" functionality (maybe a double-click)
- Add alternate voting forms besides Macromedia Flash.  
  HTML radio button or dropdowns would be an easy first step.
  AJAX (Asynchronous JavaScript and XML) would be better.
