02 Oct 2006:
------------
- Fixed bug where if ($node->voting) was always returning true when making
  decisions based on content type due to content type being a non-empty
  string.
- Setting a content type to "always" display Voting should work as expected
  now

25 Sept 2006:
-------------
- Fixed bug where voting location and show in teaser option were sharing the
  same variable.  Voting controls should now show up in the proper location.



10 July 2006:
-------------
- Updated to support Drupal 4.7.  The voting module now relies on the voting API
  module.  An upgrade script is included for migrating from the 4.6 version.  
  The voting filter and drupalvoting TinyMCE plugin have been removed since 
  the last version because as far as I know no one is using them.
 


9 Sept 2005:
------------
- Added optional attribute for bgcolor (http://drupal.org/node/28228)
- Rewrote voting_flash() function in attempt to solve vote saving problem 
  (http://drupal.org/node/30274)
- Added option for IP timeout for anonymous users.  This controls if and how
  often users with the same IP address can vote for the same thing.  Set it
  to 0 to disable.
- Added option for controlling the location of the voting control on a node.
  The options are: display above the node, display below the node, or do not display.
  Select "Do not display" if you are calling the voting module function 
  directly from your theme and/or module code.
- Misc code reformatting/commenting


13 July 2005:
-------------
- Added a voting filter so that voting controls can be added inline to a post.
- Added a drupalvoting plugin for the TinyMCE WYSIWYG editor so that voting 
controls can be added to any post (and edited) without typing the filter code.
- Changed terminology from 'voting form' to the more generic term 'voting 
control'.  This change affected the name of two functions.


5 July 2005:
------------
- Fixed table name bug
- Converted link to use drupal_get_path()
- Removed outdated source files


2 July 2005:
------------

- Converted Flash voting stars from image (raster) based to vector based.
- Added Drupal administrative options for changing Flash colors and text strings.
- Added a permission to 'show average without voting'.  For example, 
anonymous users could be allowed to see what others have voted, but not 
vote themselves unless they login.


23 June 2005:
-------------

Initial release.
