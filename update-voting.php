<?php
/**
 * Copy this file to your drupal installation root and run from a web browser
 *
 * BACK UP YOUR DATABASE FIRST!
 */


include_once 'includes/bootstrap.inc';
include_once 'includes/common.inc';
set_time_limit(60 * 60); // 1 hour
$fields = array('thumb_path' => 'thumbnail',
                'preview_path' => 'preview',
                'image_path' => '_original');

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

if (!$_POST['start']) {
	print "<p>WARNING: This script attempts to set the timeout to 60 minutes, but some server configurations may not allow this. 
			If this script times out, you may end up with duplicate and/or incomplete voting records.  I recommend backing up both 
			your 'votes' table and your 'votingapi_vote' table before running this script.  If this script times out you should reload
			the votingapi table you've backed up and try again.</p>\n";
	print '<form method="post" action="' . $_SERVER['PHP_SELF'] . '"><input type="submit" name="start" value="Start"></form>';
} else {
	if (function_exists('voting_convert_to_percent')) {
		if (function_exists('votingapi_set_vote')) {
			$result = db_query("SELECT * FROM {votes}");
			print "Converting votes";
			$i = 0;
			while ($old_vote = db_fetch_object($result)) {
				$i++; 
				if ($i > 80) { 
					print "<br />";
					$i = 0;
				}
				print "."; flush();
				$value = voting_convert_to_percent($old_vote->vote);
				$type = 'percent';
				$tag = 'vote';
				
				// add the vote to the Voting API table
				$vobj = votingapi_add_vote($old_vote->content_type, $old_vote->content_id, $value, $type, $tag, $old_vote->uid);
				
				// update the timestamp and hostname from the old data
				db_query("UPDATE {votingapi_vote} SET timestamp=%d, hostname='%s' WHERE vote_id=%d", 
					$old_vote->timestamp, $old_vote->hostname, $vobj->vote_id);
					
				// recalculate Voting API results
				votingapi_recalculate_results($old_vote->content_type, $old_vote->content_id);
			}
			print "<p>Update complete.  You should delete this script and may also remove the 'votes' table from your 
				database (always keep a backup though).</p>";
		} else {
			print "<p>The voting API module must be installed before running this update script.</p>\n";
		}
	} else {
		print "<p>The voting module must be installed before running this update script.</p>\n";
	}
}
?>
